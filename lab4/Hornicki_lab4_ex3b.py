import numpy as np
import scipy
import matplotlib.pyplot as plt

def a(x, t):
    return 0.5


def f(x, t):
    return x * (x - t)


def v(x):
    return 0


def phi(t):
    return t


def exact(x, t):
    return np.dot((x**2), t)


x_start = 0
x_end = 1

t_start = 0
t_end = 1

factor = 0.5
errors = []

for n in [50, 100, 200, 400, 800]:
    time_steps = int(n * factor)
    x = np.linspace(x_start, x_end, n + 1)
    t = np.linspace(t_start, t_end, time_steps + 1)
    h = (x_end - x_start) / n
    k = (t_end - t_start) / time_steps
    lambda_val = k / h

    boundary = np.zeros((time_steps + 1, 1))
    intermediate = np.zeros((n, 1))
    solution = np.zeros((1, 1))

    for time_step_index in range(time_steps + 1):
        boundary[time_step_index, 0] = phi(t[time_step_index])

    source = np.zeros((n, time_steps))
    for spatial_index in range(n - 1):
        for time_step_index in range(time_steps):
            source[spatial_index, time_step_index] = k * f(x[spatial_index], t[time_step_index + 1])

    for spatial_index in range(n - 1):
        solution = np.block([[solution], [v(x[spatial_index + 1])]])

    for time_step_index in range(time_steps):
        for spatial_index in range(n):
            intermediate[spatial_index] = lambda_val * a(x[spatial_index + 1], t[time_step_index + 1])

        diagonal = 1 + intermediate
        off_diagonal = np.block([[0], [-intermediate[0 : n - 1]]])
        block_structure = np.array([0, 1])

        block_matrix = np.block([[diagonal, off_diagonal]]).T
        A = scipy.sparse.spdiags(block_matrix, block_structure, n, n).A
        block_vector = intermediate[0] * np.block([[np.zeros((n - 1, 1))], [boundary[time_step_index]]])

        updated_source = source[:, time_step_index].reshape((n, 1))
        inverse_A = np.linalg.inv(A)
        solution = np.dot(inverse_A, (solution + block_vector + updated_source))

    solution = np.block([[solution], [phi(t[time_steps])]])
    exact_solution = exact(x[0 : n + 1], t[time_steps])

    # Plots
    plt.figure()
    plt.plot(x, solution, "r.", label="Crank-Nicolson")
    plt.plot(x, exact_solution, "b-", label="Exact")
    plt.legend()
    plt.title(f"{n}-grid Crank-Nicolson")
    plt.grid()

plt.show()
