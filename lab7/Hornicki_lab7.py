import numpy as np
import matplotlib.pyplot as plt


for n in [5, 10, 20, 40]:
    h = 2 * np.pi / n
    s = np.arange(0, 2 * np.pi + h, h)

    # example 1
    #     input:
    #         x**2 + y**2 = r**2
    #     output:
    #         x = r * np.cos(s)
    #         y = r * np.sin(s)

    def ex1_x(s, r):
        return r * np.cos(s)

    def ex1_y(s, r):
        return r * np.sin(s)

    r = 3

    ex1_points_x = np.array(ex1_x(s, r))
    ex1_points_y = np.array(ex1_y(s, r))
    plt.figure()
    plt.xlim([-(r+1), r+1])
    plt.ylim([-(r+1), r+1])
    plt.title(f"ex1 n={n}")
    plt.plot(ex1_points_x, ex1_points_y, "--", marker="o")

    # example 2:
    #     input:
    #         x**2 / a**2 + y**2 / b**2 = 1
    #         a = 3
    #         b = 2
    #     output:
    #         x = 3 * np.cos(s)
    #         y = 2 * np.sin(s)

    def ex2_x(s):
        return 3 * np.cos(s)

    def ex2_y(s):
        return 2 * np.sin(s)

    ex2_points_x = np.array(ex2_x(s))
    ex2_points_y = np.array(ex2_y(s))
    plt.figure()
    plt.xlim([-4, 4])
    plt.ylim([-4, 4])
    plt.title(f"ex2 n={n}")
    plt.plot(ex2_points_x, ex2_points_y, "--", marker="o")

plt.show()


#2nd part

def x(s, r):
    return r * np.cos(s)

def y(s, r):
    return r * np.sin(s)

def x_dx(s,r):
    return r * -np.sin(s)
def y_dx(s,r):
    return r * np.cos(s)

k = np.arange(0,4+1,1)

x_k = np.cos(2*k*np.pi/5)
y_k = np.sin(2*k*np.pi/5)

# n-> = [y_dx(s0), x_dx(s0)] / np.sqrt(x_dx(s0)**2 + y_dx(s0)**2) #dy
# v-> = [x_dx(s0), y_dx(s0)] / np.sqrt(x_dx(s0)**2 + y_dx(s0)**2) #dx