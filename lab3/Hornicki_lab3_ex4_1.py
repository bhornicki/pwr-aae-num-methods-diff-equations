import numpy as np
import scipy
import matplotlib.pyplot as plt

a = 0
b = 1
y0 = 0
y1 = 0
k = -1

# y''
def p(x):
    return 1

# y(x)
def q(x):
    return np.exp(x)

# right side
def f(x):
    return (np.pi**2 + np.exp(x)) * np.sin(np.pi * x)

# exact solution
def y(x):
    return np.sin(np.pi * x)

# exact derivative
def ydx(x):
    return np.pi * np.cos(np.pi * x)


error_y = np.zeros(5)
error2 = np.array([])
error_h = np.zeros(5)
for index, n in enumerate([50, 100, 200, 400, 800]):
    k += 1
    x = np.linspace(a, b, n + 1)
    h = (b - a) / n

    matrix = np.zeros((n - 1, n - 1))

    v0 = np.zeros(n - 1)
    v1 = np.zeros(n - 2)
    v2 = np.zeros(n - 2)
    for i in range(n - 1):
        xr = x[i + 1]
        v0[i] = 1 / h * (p(xr + h / 2) + p(xr + h / 2)) + h / 3 * (q(xr + h / 2) + q(xr - h / 2))

    v0 = np.transpose(v0)

    for i in range(n - 2):
        xr = x[i + 2]
        v2[i] = -1 / h * p(xr - h / 2) + h / 6 * q(xr - h / 2)
        xr = x[i + 1]
        v1[i] = -1 / h * p(xr - h / 2) + h / 6 * q(xr - h / 2)

    v0 = v0.reshape((n - 1, 1))
    v1 = np.transpose(np.block([v1, 0])).reshape((n - 1, 1))
    v2 = np.transpose(np.block([0, v2])).reshape((n - 1, 1))
    D = np.array([-1, 0, 1])

    block = np.vstack([v1.ravel(), v0.ravel(), v2.ravel()])
    matrix = scipy.sparse.spdiags(block, D, n - 1, n - 1)

    fb = np.zeros(n - 1)
    for i in range(n - 1):
        xr = x[i + 1]
        fb[i] = h / 2 * (f(xr + h / 2) + f(xr - h / 2))

    matrix = matrix.A

    P, L, U = scipy.linalg.lu(matrix)

    fb = np.dot(P, np.transpose(fb))
    z = np.dot(np.linalg.inv(L), fb)
    yp = np.dot(np.linalg.inv(U), z).reshape((n - 1, 1))

    ypp = np.block([[y0], [yp], [y1]])

    yd = y(x).reshape((n + 1, 1))

    error_y[k] = np.max(np.abs(ypp - yd))
    error_h[k] = h

    print(f"{n} elements, error={error_y[k]}")

    plot1 = plt.figure(index)
    plt.plot(x, ypp, ".", label="Estimated")
    plt.plot(x, y(x), ".", label="Exact")
    plt.legend()
    plt.xlabel("t")
    plt.ylabel("Y")
    plt.title(f"{n}-element Ritz-Galerkin")
    plt.grid()

    plot1 = plt.figure(index + 6)
    plt.plot(x, np.abs(ypp - yd), ".", color="blue", label="error_y")
    plt.legend()
    plt.xlabel("t")
    plt.ylabel("error_y")
    plt.title(f"{n}-element error")
    plt.grid()

coefficients = np.polyfit(error_h, error_y, 2)
poly = np.poly1d(coefficients)
x_values = np.linspace(np.min(error_h), np.max(error_h), 500)
y_values = poly(x_values)

plt.figure()
plt.plot(error_h, error_y, "o")
plt.plot(x_values, y_values, "r-")
plt.xlabel("h")
plt.ylabel("error")
plt.grid(True)
plt.title("Estimated error function")
plt.show()
