import numpy as np
import matplotlib.pyplot as plt


def f(x):
    return np.sin(np.pi * x)


def q(t):
    return 0


def exact(x, t):
    return np.sin(np.pi * x) * np.cos(np.pi * t)


def forward_difference(h, k, T):
    x = np.arange(0, 1 + h, h)
    t = np.arange(0, T + k, k)
    u = np.zeros((len(t), len(x)))

    # initial conditions
    u[0, :] = f(x)

    # boundary conditions
    u[:, 0] = q(t)
    u[:, -1] = q(t)

    r = k**2 / h**2

    # first step with du/dt == 0
    for j in range(1, len(x) - 1):
        u[1, j] = u[0, j] + 0.5 * r * (u[0, j + 1] - 2 * u[0, j] + u[0, j - 1])

    # forward-difference method
    for n in range(1, len(t) - 1):
        for j in range(1, len(x) - 1):
            u[n + 1, j] = (
                2 * (1 - r) * u[n, j] - u[n - 1, j] + r * (u[n, j + 1] + u[n, j - 1])
            )

    return x, t, u


h_values = [0.1, 0.05, 0.05]
k_values = [0.05, 0.1, 0.05]
T = 0.5

for h, k in zip(h_values, k_values):
    x, t, u = forward_difference(h, k, T)

    u_exact = exact(x, T)

    plt.figure()
    plt.plot(x, u[-1, :], label="Forward difference")
    plt.plot(x, u_exact, label="Exact")
    plt.title(f"h = {h}, k = {k}")
    plt.legend(loc="upper right")

plt.show()
