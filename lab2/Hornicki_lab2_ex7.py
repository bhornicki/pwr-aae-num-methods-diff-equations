import numpy as np
import matplotlib.pyplot as plt
def f(t, y):
    return -100 * (y - np.cos(t)) - np.sin(t)

def f_exact(t):
    return np.cos(t)

# Set the initial condition and time interval
y0 = 1
t0 = 0
t1 = 1

# explicit
n = 1
while True:
    t = np.linspace(t0, t1, n)
    h = (t1-t0)/n
    y = np.zeros_like(t)
    y[0] = y0
    
    for i in range(len(t)-1):
        y[i+1] = y[i] + h*f(t[i], y[i])

    error = np.abs(y[-1] - f_exact(t1))

    y_exact = []
    for i in t:
        y_exact.append(f_exact(i))

    plt.plot(t, y, '.', label=f'Explicit Euler {n}')
    plt.plot(t, y_exact, '.', label=f'Exact {n}')
    plt.legend()
    plt.show()
    
    if error < 0.001:
        print(f"Explicit Euler at n {n} and h {h}: {error:.6f}")
        # break
    n+=1

#implicit
n = 3
while True:
    t = np.linspace(t0, t1, n)
    h = (t1-t0)/n
    y = np.zeros_like(t)
    y[0] = y0

    for i in range(len(t)-1):
        # y[i] = y[i+1] - h*f(t[i+1], y[i+1])
        # y[i] = y[i+1] - h * (-100 * (y[i+1] - np.cos(t[i+1])) - np.sin(t[i+1]))
        # y[i] = y[i+1] - h * (-100 * y[i+1] + 100 * np.cos(t[i+1]) - np.sin(t[i+1]))
        # y[i] = y[i+1] + h * 100 * y[i+1] - h * 100 * np.cos(t[i+1]) + h * np.sin(t[i+1])
        # y[i] = (1 + h * 100) * y[i+1] - h * 100 * np.cos(t[i+1]) + h* np.sin(t[i+1])
        # (1 + h * 100) * y[i+1] = y[i] + h * 100 * np.cos(t[i+1]) - h* np.sin(t[i+1])
        y[i+1] = (y[i] + h * 100 * np.cos(t[i+1]) - h* np.sin(t[i+1])) / (1 + h * 100)
        
    y_exact = []
    for i in t:
        y_exact.append(f_exact(i))

    plt.plot(t, y, '.', label=f'Explicit Euler {n}')
    plt.plot(t, y_exact, '.', label=f'Exact {n}')
    plt.legend()
    plt.show()

    error = np.abs(y[-1] - f_exact(t1))
    if error < 0.001:
        print(f"Implicit Euler at n {n} and h {h}: {error:.6f}")
        # break
    n+=1
