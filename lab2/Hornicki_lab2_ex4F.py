import numpy as np
import matplotlib.pyplot as plt
import math

t_zero = 0
t_first = 0.1
t_second = 1

u1_0 = 1/3
u2_0 = 1/3

h_first = 0.01
h_second = 0.1

A = np.array([[32, 66], [-66, -133]])

t1 = np.arange(t_zero, t_first, h_first)
t11 = np.arange(t_zero+h_first, t_first, h_first)
t2 = np.arange(t_first, t_second+h_second, h_second)
t_whole = np.concatenate((t1, t2))

# 
# Solving methods
# 

def f(t):
    return np.array([[2/3 * t + 2/3], [-1/3 * t - 1/3]])

def exact(t):
    return np.array([[t*2/3 + math.exp(-t)*2/3 - math.exp(-100*t)*1/3], 
                    [-t*1/3 - math.exp(-t)*1/3 + math.exp(-100*t)*2/3]])


def explicit(previous, h): 
    return previous + h * np.dot(A, previous) + f(t)

def implicit(previous, h): 
    return np.dot(np.linalg.inv(np.eye(2) - h*A),
            previous + h * f(t + h))

def trapezoidal(prev, h): 
    return np.dot(np.linalg.inv(np.eye(2) - (h/2)*A),
            prev + (h/2) * (np.dot(A, prev) + f(t) + f(t+h)))

def runge_kutta(prev, h):
    def F_Runge_Kutta(u, f_t, A):
        return np.dot(A, u) + f_t

    k1 = F_Runge_Kutta(prev, f(t), A) * h
    k2 = F_Runge_Kutta(prev+k1/2, f(t+h/2), A)*h
    k3 = F_Runge_Kutta(prev+k2/2, f(t+h/2), A)*h
    k4 = F_Runge_Kutta(prev+k3, f(t+h), A)*h

    return prev + (k1 + 2*k2 + 2*k3 + k4)/6


# 
# Helper functions
# 

def split(vec):
    u1 = []
    u2 = []
    for vec in vec:
        u1.append(vec[0])
        u2.append(vec[1])
    return [u1, u2]

def method(function):
    U_vec = []
    U_vec.append(np.array([[u1_0], [u2_0]]))
    for t in t11:
        U_vec.append(function(U_vec[-1], h_first))
    for t in t2:
        U_vec.append(function(U_vec[-1], h_second))
    return U_vec

#
# Construct exact solution
#

exac_vec = []
for t in t_whole:
    exac_vec.append(exact(t))
[ u1_exac, u2_exac ] = split(exac_vec)

#
# Main part
#

methods = [
    ["Implicit Euler", implicit],
    ["Explicit Euler", explicit],
    ["Trapezoidal", trapezoidal],
    ["Runge-Kutta", runge_kutta],
]

figure = 0
for name, algorithm in methods:
    vec = method(algorithm)

    # plot values
    plt.figure(figure)
    figure +=1
    [u1,u2] = split(vec)

    plt.plot(t_whole, u1, '.', color="darkblue", label=f'{name} u1')
    plt.plot(t_whole, u2, '.', color="blue", label=f'{name} u2')
    
    plt.plot(t_whole, u1_exac, '.', color="green", label='exact u1')
    plt.plot(t_whole, u2_exac, '.', color='lightgreen', label='exact u2')
    plt.legend()

    # plot errors
    u1_diff = []
    u2_diff = []
    for item1, item2 in zip(vec, exac_vec):
        diff = abs(item1-item2)
        u1_diff.append(diff[0])
        u2_diff.append(diff[1])

    plt.figure(figure)
    figure +=1
    plt.plot(t_whole, u1_diff, '.', label=f'{name} error u1')
    plt.plot(t_whole, u2_diff, '.', label=f'{name} error u2')
    plt.legend()

plt.show()
