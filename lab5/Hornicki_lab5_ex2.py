import numpy as np
import scipy 
import matplotlib.pyplot as plt


def v(x):
    return x * (2 - x)


x0 = 0
x1 = 2
x_range = x1 - x0
t0 = 0
t1 = 0.5
r_constant = 1
error_results = []
figure_count = 0

for num_points in [20, 40, 80, 160, 320]:
    h = (x1 - x0) / num_points
    k = (1 / 2) * r_constant * h**2
    m = int((t1 - t0) / k)

    x = np.linspace(x0, x1, num_points + 1)
    t = np.linspace(t0, t1, m + 1)

    ux = v(x[1:num_points])

    lambda_constant = k / h**2
    main_diagonal = -2 * np.ones(num_points - 1)
    lower_diagonal = np.ones(num_points - 2)
    upper_diagonal = np.ones(num_points - 2)

    A = scipy.sparse.diags([lower_diagonal, main_diagonal, upper_diagonal], [-1, 0, 1])
    identity = np.identity(num_points - 1)
    B = identity + lambda_constant * A.toarray()

    for _ in range(m):
        ux = np.dot(B, ux)

    solution = np.concatenate(([0], ux, [0]))

    harmonic_index = 1
    fourier_coefficients = []
    fourier_solution = np.zeros(num_points + 1)
    final_time_level = t1

    for harmonic_index in range(1, 6):
        g = lambda x: x * (2 - x) * np.sin(np.pi * harmonic_index * x / x_range)
        fourier_coefficients.append(scipy.integrate.quad(g, 0, 2)[0])
        fourier_solution += (
            fourier_coefficients[-1]
            * np.sin(np.pi * harmonic_index * x / x_range)
            * np.exp(
                -(harmonic_index**2) * np.pi**2 / x_range**2 * final_time_level
            )
        )

    plt.figure()
    plt.plot(x, fourier_solution, label="Fourier Solution")
    plt.plot(x, solution, label="Approximate")
    plt.legend()
    plt.title(f"num_points={num_points}")
plt.show()
